# npm-from-file
Installs Node.js packages via npm from text files

Usage:

```sh
sh npm-from-file.sh examples.txt
```
