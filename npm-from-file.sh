#!/bin/sh

  GREEN='\033[0;32m'
  NC='\033[0m'

  #   egrep filters files for regex patterns
  ##  -c count the number of lines
  ##  -v inverts the regex pattern
  ##  -e selects the regex pattern
  ##  -h no filename gets appended to output if several files are provided
  ### ^# filters lines starting with #
  ### ^$ filters empty lines ($ represents the line break)
  ### ^-g filters lines starting with -g
  # $@ represents the script's argument list
  # -ge returns true if expression beforehand is greater or equal than expression after
  regex_global='(^-g)'
  regex_project='(^#|^$|^-g)'
  written_globals=0
  colors_available=$(tput colors 2> /dev/null)
  colortest_result=$?

  printcolored () {
    if [ $colortest_result = 0 ] && [ $colors_available -gt 2 ]; then
        echo -e "${1}${2}${NC}\n"
    else
        echo $2
    fi
  }

  if [ $(egrep -che $regex_global $@ | awk '{s+=$1} END {print s}') -ge 1 ]; then
    printcolored ${GREEN} "installing global packages"
    egrep -he $regex_global $@  | xargs npm i --silent
    written_globals=1
  fi
  if [ $(egrep -chv $regex_project $@ | awk '{s+=$1} END {print s}') -ge 1 ]; then
    if [[ $written_globals -ge 1 ]]; then
      echo "----------------------------------------------------------"
    fi
    printcolored ${GREEN} "installing projectspecific packages"
    egrep -hv $regex_project $@ | xargs npm i --silent
  fi
